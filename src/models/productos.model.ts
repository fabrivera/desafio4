import { model, Schema } from 'mongoose'
import IProductos from '../interfaces/productos.interface'

const ProductoSchema = new Schema({
    nombre: {
        type: String,
        required: [true, 'El nombre del producto es requerido']
    },
    precio: {
        type: Number,
        required: [true, 'El precio de producto es requerido']
    },
    stock: Number,
    estado: {
        type: String,
        enum: ['nuevo', 'usado']
    },
    ventas_id: [{
        type: Schema.Types.ObjectId,
        ref: 'Venta'
    }]
},{
    timestamps: { createdAt: true, updatedAt: true }
})

export default model<IProductos>('Producto', ProductoSchema)