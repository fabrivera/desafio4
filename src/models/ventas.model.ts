import { model, Schema } from 'mongoose'
import IVentas from '../interfaces/ventas.interface'

const VentaSchema = new Schema({
    forma_de_pago: {
        type: String,
        enum: ['contado', 'tarjeta']
    },
    precio_total: {
        type: Number,
        required: [true, 'El precio de producto es requerido']
    },
    estado: {
        type: String,
        enum: ['APROBADA', 'ANULADA']
    },
    persona_id: {
        type: Schema.Types.ObjectId,
        ref: 'Persona'
    },
    productos: [{
        type: Schema.Types.ObjectId,
        ref: 'Producto'
    }]
},{
    timestamps: { createdAt: true, updatedAt: true }
})

export default model<IVentas>('Venta', VentaSchema)