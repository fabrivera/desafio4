import { model, models, Schema } from 'mongoose'
import IPersonas from '../interfaces/personas.interface'
import bcrypt from 'bcrypt'

const PersonaSchema = new Schema({
    nombreCompleto: {
        type: String,
        required: [true, 'El nombreCompleto es obligatorio']
    },
    email: {
        type: String,
        unique: true,
        required: [true, 'El email es obligatorio y único'],
        lowercase: true,
        trim: true
    },
    contraseña: {
        type: String,
        required: [true, 'La contraseña es obligatoria']
    },
    telefono: String,
    rol: {
        type: String,
        required: [true, 'El rol es obligatorio. Valores posibles: admin/cliente.'],
        lowercase: true,
        enum: ['admin', 'cliente']
    }
},{
    timestamps: { createdAt: true, updatedAt: true }
})

PersonaSchema.pre<IPersonas>('save', async function (next) {
    if (!this.isModified('contraseña')) return next()

    const salt = await bcrypt.genSalt(10)
    const hash = await bcrypt.hash(this.contraseña, salt)

    this.contraseña = hash
    next()
})

PersonaSchema.path('email').validate(async(email: string)=>{
    const emailcount = await models.Persona.countDocuments({email})
    return !emailcount
}, 'Email already exits')

PersonaSchema.set('toJSON', {
    transform: (document, returnedObject) => {
        delete returnedObject.__v
        delete returnedObject.contraseña
    }
})

PersonaSchema.methods.compararContraseña = async function (contraseña: string): Promise<boolean> {
    return await bcrypt.compare(contraseña, this.contraseña)
}

export default model<IPersonas>('Persona', PersonaSchema)