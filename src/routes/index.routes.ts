import { Router } from 'express'
import personasRouter from './personas.routes'
import productosRouter from './productos.routes'
import ventasRouter from './ventas.routes'

const router = Router()

router.use('/usuarios', personasRouter)
router.use('/ventas', ventasRouter)
router.use('/productos', productosRouter)

export default router