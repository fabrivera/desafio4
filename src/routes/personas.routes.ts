import { Router } from 'express'
import personaCtrl from '../controllers/personas.controller'
import { validateFields } from '../middlewares/validate-fields'

const router = Router()
const { index, show, create, update, destroy } = personaCtrl

router.route('/')
    .get(index)
    .post(validateFields, create)

router.route('/:id')
    .get(show)
    .put(update)
    .delete(destroy)

export default router