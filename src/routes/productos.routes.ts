import { Router } from 'express'
import productoCtrl from '../controllers/productos.controller'
import { validateFields } from '../middlewares/validate-fields'

const router = Router()
const { index, show, create, update, destroy } = productoCtrl

router.route('/')
    .get(index)
    .post(validateFields, create)

router.route('/:id')
    .get(show)
    .put(update)
    .delete(destroy)

export default router