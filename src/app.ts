import express, { Application } from 'express'
import hpp from 'hpp'
import helmet from 'helmet'
import apiRouter from './routes/index.routes'

const app:Application = express()

// Settings
app.set('port', process.env.PORT || 5000)

// Middlewares
app.use(hpp())
app.use(helmet())
app.use(express.json())

// Routes
app.use('/', apiRouter)


export default app