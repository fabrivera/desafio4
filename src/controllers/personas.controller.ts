import Persona from '../models/personas.model'
import { IUpdatePersonas } from '../interfaces/personas.interface'
import IController from '../interfaces/controller.interface'

const personaCtrl:IController = {}

// Función index devuelve todas las personas de la base de datos
personaCtrl.index = async (req, res) => {
    try {
        const result = await Persona.find({})
        return res.status(200).json(result)
    }
    catch {
        return res.status(500).json({msg: 'Error con la conección a la colección, no hay resultados para mostrar'})
    }
}

// Función show devuelve una persona por path param id
personaCtrl.show = async (req, res) => {
    const _id = req?.params?.id
    
    if (!_id)
        return res.status(400).json({msg: `Debe enviar el ID como path params`})

    try {
        const result = await Persona.findById({_id})
        return res.status(200).json(result)
    }
    catch {
        return res.status(500).json({msg: `No se pudo recuperar la persona con id: ${_id}`})
    }
}

// Functón create almacena un nuevo registro en la colección
personaCtrl.create = async (req, res) => {
    const { nombre, email, contraseña, telefono, rol } = req?.body

    if (
        !nombre ||
        !email ||
        !contraseña ||
        !rol
    ) return res.status(400).json({msg: `Parametros enviados incorrectos, debe enviar nombre, email, contraseña, telefono, rol: [admin, cliente]`})

    try {
        const newPersona = new Persona({
            nombreCompleto: nombre,
            email,
            contraseña,
            telefono,
            rol
        })
        await newPersona.save()
        return res.status(201).json({msg: 'Persona creada'})
    }
    catch {
        return res.status(500).json({msg: 'No se pudo crear Persona en la colección'})
    }
}

// Función update edita el registro Persona por id
personaCtrl.update = async (req, res) => {
    const { nombre, email, contraseña, telefono, rol } = req?.body
    const _id = req?.params?.id
    const updatePersona:IUpdatePersonas = {}

    if (nombre) updatePersona.nombreCompleto = nombre
    if (email) updatePersona.email = email
    if (contraseña) updatePersona.contraseña = contraseña
    if (telefono) updatePersona.telefono = telefono
    if (rol) updatePersona.rol = rol

    try {
        const persona = await Persona.findByIdAndUpdate({_id},{...updatePersona})
        if (!persona) return res.status(404).json({msg: `Persona no encontrada con id: ${_id}`})
        return res.status(201).json({msg: 'Persona actualizada'})
    }
    catch {
        return res.status(500).json({msg: `No se pudo actualizar la persona con id: ${_id}`})
    }

}

// Función destroy elimina un usuario
personaCtrl.destroy = async (req, res) => {
    const _id = req?.params?.id
    try {
        const persona = await Persona.findByIdAndDelete({_id})
        if (!persona) return res.status(404).json({msg: `Persona no encontrada con id: ${_id}`})
        return res.status(201).json({msg: `Se eliminó la persona ${persona.nombreCompleto}`, persona})
    }
    catch {
        return res.status(500).json({msg: `No se pudo eliminar la persona con id: ${_id}`})
    }

}

export default personaCtrl