import Venta from "../models/ventas.model"
import Producto from "../models/productos.model"
import { IUpdateVentas } from "../interfaces/ventas.interface"
import IController from "../interfaces/controller.interface"

const ventasCtrl: IController = {}

// Función index devuelve todas las ventas de la base de datos
ventasCtrl.index = async (req, res) => {
    try {
        const result = await Venta.find({})
        return res.status(200).json(result)
    }
    catch {
        return res.status(500).json({msg: 'Error con la conección a la colección, no hay resultados para mostrar'})
    }
}

// Función show devuelve un producto por path param id junto a sus ventas y quienes las realizaron
ventasCtrl.show = async (req, res) => {
    const _id = req?.params?.id
    
    if (!_id)
        return res.status(400).json({msg: `Debe enviar el ID como path params`})

    try {
        const result = await Venta.findById({_id})
            .populate({
                path: 'persona_id',
                select: {'_id': 1, 'nombreCompleto': 1, 'email': 1}
            })
            .populate([{
                path: 'productos',
                select: {'_id': 1, 'nombre': 1, 'precio': 1}
            }])
        return res.status(200).json(result)
    }
    catch {
        return res.status(500).json({msg: `No se pudo recuperar la venta con id: ${_id}`})
    }
}

// Crear una nueva venta
ventasCtrl.create = async (req, res) => {
    const { forma_de_pago, persona_id, productos } = req?.body

    if (
        !forma_de_pago ||
        !persona_id ||
        !productos &&
        productos !== typeof Array
    ) return res.status(400).json({msg: `Compra rechazada por falta de datos`})

    try {
        const newVenta = new Venta({
            forma_de_pago,
            persona_id,
            estado: 'APROBADA',
            precio_total: 0,
            productos
        })

        await Promise.all(productos.map(async (_id:string) => {
            const product = await Producto.findOne({_id})
            const stock = Number(product?.stock)
            if (stock === NaN ||
                stock <= 0 ||
                Number(product?.precio) === NaN 
            ) newVenta.estado = 'ANULADA'
        }))

        if (newVenta.estado === 'APROBADA') {
            await Promise.all(productos.map(async (_id:string) => {
                const product = await Producto.findOne({_id})
                const stock = Number(product?.stock)
                if (newVenta.precio_total === undefined) return res.status(500)
                newVenta.precio_total = newVenta.precio_total + Number(product?.precio) 
                const ventas_id = product?.ventas_id?.concat(newVenta._id)
                await Producto.findByIdAndUpdate({_id}, {
                    ventas_id,
                    stock: stock - 1
                })
            }))
        }
        await newVenta.save()
        return res.status(201).json({msg: newVenta})
    }
    catch {
        return res.status(500).json({msg: 'No se pudo crear el producto en la colección'})
    }
}

// Función update edita el registro Venta
ventasCtrl.update = async (req, res) => {
    const { forma_de_pago, precio_total, persona_id, estado } = req?.body
    const _id = req?.params?.id
    const updateVenta:IUpdateVentas = {}

    if (forma_de_pago) updateVenta.forma_de_pago = forma_de_pago
    if (precio_total) updateVenta.precio_total = precio_total
    if (estado) updateVenta.estado = estado
    if (persona_id) updateVenta.persona_id = persona_id

    try {
        const venta = await Venta.findByIdAndUpdate({_id},{...updateVenta})
        if (!venta) return res.status(404).json({msg: `Venta con id ${_id} no encontrada`})
        return res.status(201).json({msg: 'Venta actualizada'})
    }
    catch {
        return res.status(500).json({msg: `No se pudo actualizar la venta con id: ${_id}`})
    }
}

// Función destroy elimina un usuario
ventasCtrl.destroy = async (req, res) => {
    const _id = req?.params?.id
    try {
        const venta = await Venta.findById({_id})
        if (!venta?.productos) return res.status(404).json({msg: `Producto con id ${_id} no encontrado`})
        await Promise.all(venta?.productos.map(async producto_id => {
            const list = await Producto.findById({_id: producto_id})
            if (!list?.ventas_id || list?.ventas_id === undefined) {
                return res.status(500).json({msg: 'No se pudo eliminar el productos de las ventas'})
            }
            const newList = list.ventas_id.filter(ventaId => ventaId.toString() !== _id)
            await Producto.findByIdAndUpdate({_id: producto_id}, {ventas_id: newList})
        }))

        await Venta.findByIdAndDelete({_id})
        return res.status(201).json({msg: `Se eliminó la venta con id ${venta._id}`, venta})
    }
    catch (err){
        return res.status(500).json({msg: `No se pudo eliminar el producto con id: ${_id}`,
    error: err})
    }
}

export default ventasCtrl