import Producto from "../models/productos.model"
import Venta from "../models/ventas.model"
import { IUpdateProductos } from "../interfaces/productos.interface"
import IController from "../interfaces/controller.interface"

const productoCtrl: IController = {}

// Función index devuelve todos los productos de la base de datos
productoCtrl.index = async (req, res) => {
    try {
        const result = await Producto.find({})
        return res.status(200).json(result)
    }
    catch {
        return res.status(500).json({msg: 'Error con la conección a la colección, no hay resultados para mostrar'})
    }
}

// Función show devuelve un producto por path param id junto a sus ventas y quienes las realizaron
productoCtrl.show = async (req, res) => {
    const _id = req?.params?.id
    
    if (!_id)
        return res.status(400).json({msg: `Debe enviar el ID como path params`})

    try {
        const result = await Producto.findById({_id}).populate({
            path: 'ventas_id',
            select: {'_id': 1, 'persona_id': 1},
            populate: {
                path: 'persona_id',
                select: {
                    '_id': 1,
                    'nombreCompleto': 1,
                    'email': 1
                }
            }
        })
        return res.status(200).json(result)
    }
    catch {
        return res.status(500).json({msg: `No se pudo recuperar el producto con id: ${_id}`})
    }
}

// Functón create almacena un nuevo registro en la colección
productoCtrl.create = async (req, res) => {
    const { nombre, precio, stock = 0, estado } = req?.body

    if (
        !nombre ||
        !precio ||
        !estado
    ) return res.status(400).json({msg: `Parametros enviados incorrectos, debe enviar nombre, precio, estado: [nuevo, usado], también se puede enviar el stock`})

    try {
        const newProducto = new Producto({
            nombre,
            precio,
            stock,
            estado
        })
        await newProducto.save()
        return res.status(201).json({msg: 'Producto creado'})
    }
    catch {
        return res.status(500).json({msg: 'No se pudo crear el producto en la colección'})
    }
}

// Función update edita el registro Producto
productoCtrl.update = async (req, res) => {
    const { nombre, precio, stock, estado } = req?.body
    const _id = req?.params?.id
    const updateProducto:IUpdateProductos = {}

    if (nombre) updateProducto.nombre = nombre
    if (precio) updateProducto.precio = precio
    if (stock) updateProducto.stock = stock
    if (estado) updateProducto.estado = estado

    try {
        const producto = await Producto.findByIdAndUpdate({_id},{...updateProducto})
        if (!producto) return res.status(404).json({msg: `Producto con id ${_id} no encontrado`})
        return res.status(201).json({msg: 'Producto actualizado'})
    }
    catch {
        return res.status(500).json({msg: `No se pudo actualizar el producto con id: ${_id}`})
    }
}

// Función destroy elimina un usuario
productoCtrl.destroy = async (req, res) => {
    const _id = req?.params?.id
    try {
        const producto = await Producto.findById({_id})
        if (!producto?.ventas_id) return res.status(404).json({msg: `Producto con id ${_id} no encontrado`})
        await Promise.all(producto?.ventas_id.map(async venta_id => {
            const list = await Venta.findById({_id: venta_id})
            if (!list?.productos) return res.status(500).json({msg: 'No se pudo eliminar el productos de las ventas'})
            const newList = list.productos.filter((producto_id) => producto_id.toString() !== _id)
            await Venta.findByIdAndUpdate({_id: venta_id}, {productos: newList})
        }))

        await Producto.findByIdAndDelete({_id})
        return res.status(201).json({msg: `Se eliminó el producto ${producto.nombre}`, producto})
    }
    catch {
        return res.status(500).json({msg: `No se pudo eliminar el producto con id: ${_id}`})
    }
}

export default productoCtrl