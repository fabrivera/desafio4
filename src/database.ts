import mongoose from 'mongoose'

const collection:string = 'desafio4'

mongoose.connect(process.env.MONGODB_URI+collection)

const connection = mongoose.connection

connection.once('open', () => {
    console.log('🟢 The database is connected.')
})

connection.on('error', err => {
    console.error(`🔴 Unable to connect to the database: ${err}.`)
    process.exit(0)
})