import { Request, Response, NextFunction } from 'express'

export default interface IController {
    [key:string]: (req:Request, res:Response, next?:NextFunction ) => Promise<Response>
}