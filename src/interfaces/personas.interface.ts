import { Document } from 'mongoose'

export default interface IPersonas extends Document {
    nombreCompleto: string
    email: string
    contraseña: string
    telefono?: string
    rol: string

    compararContraseña: (contraseña: string) => Promise<boolean>
}

export interface IUpdatePersonas {
    nombreCompleto?: string
    email?: string
    contraseña?: string
    telefono?: string
    rol?: string
}