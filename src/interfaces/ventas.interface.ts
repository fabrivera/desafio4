import { Document, ObjectId } from 'mongoose'

export default interface IVentas extends Document {
    forma_de_pago?: string
    precio_total?: number
    estado?: string
    persona_id?: ObjectId
    productos?: [ObjectId]
}

export interface IUpdateVentas {
    forma_de_pago?: string
    precio_total?: number
    estado?: string
    persona_id?: ObjectId
    productos?: [ObjectId]
}